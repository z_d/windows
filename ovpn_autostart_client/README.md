### Example auto-run OpenVpn Client with use PowerShell and Task Scheduler.


- Create PowerShell-script. Or [download](https://gitlab.com/z_d/windows/-/blob/master/ovpn_autostart_client/runovpnclient.ps1) and edit for youself.

```
$godir = "C:\Program Files\OpenVPN\bin"
$runovpn = '.\openvpn.exe --config "C:\Program Files\OpenVPN\config\client.ovpn"'

cd $godir

Invoke-expression $runovpn | Out-File -FilePath "C:\Users\user\Desktop\runlog.txt"

```

- Change the variables for yourself.

- Select a directory and save the script in it.

- Create directory OpenVpnClientTask:  Sheduler(local) --> Task sheduler library --> Windows --> OpenVpnClient 

- Download [xml-file](https://gitlab.com/z_d/windows/-/blob/master/ovpn_autostart_client/Auto-start.xml).

- Edit the xml-file for youself.

- Add xml-file in Task Sheduler.

- Reboot PC and test !



