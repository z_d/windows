#Источник: https://vmblog.ru/ustanovka-servera-openssh-na-windows-server/
#Запустить PowerShell от имени администратора

#Разрешить выполненние любых скриптов:
Set-ExecutionPolicy Unrestricted

#Запустить срипт:
.\install_sshd.ps1

#Установка Open SSH:
choco install openssh -params '"/SSHServerFeature /KeyBasedAuthenticationFeature"' –y

#Создать правило в брендмауэре:
New-NetFirewallRule -Protocol TCP -LocalPort 22 -Direction Inbound -Action Allow -DisplayName SSH

#Для перезагрузки переменных окружения, выполните команду:
refreshenv
