import winrm

host = 'pc1.example.com'
domain = 'example.com'
user = 'Username'
password = 'Password'

session = winrm.Session(host, auth=('{}@{}'.format(user,domain), password), transport='ntlm')

command = session.run_cmd('ipconfig', ['/all']) # To run command in cmd

print(command.std_out.decode('utf-8'))
