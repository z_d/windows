import winrm

script = """net stop spooler
 ping 127.0.0.1 -n 3 >nul
 del %systemroot%\system32\spool\printers\*.* /F /Q /S
 ping 127.0.0.1 -n 3 >nul
 net start spooler
 msg * Служба печати перезагружена"""

host = 'pc.domain'
domain = 'domain'
user = 'User'
password = 'Password'

session = winrm.Session(host, auth=('{}@{}'.format(user,domain), password), transport='ntlm')

command = session.run_ps(script) # To run command in cmd

print(command.std_out.decode('866'))
